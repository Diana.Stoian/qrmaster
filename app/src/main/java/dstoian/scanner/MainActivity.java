package dstoian.scanner;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import dstoian.scanner.Reader.BarcodeCaptureActivity;


public class MainActivity extends AppCompatActivity {

    TextView decText, decContent;
    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeMain";
    private ShareActionProvider mShareActionProvider;
    Toolbar myToolbar;
    static final String DECODED_TEXT = "decodedText";
    private String decodedText = "";
    private static final int SELECTED_PICTURE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        decText = (TextView) findViewById(R.id.decText);
        decContent = (TextView) findViewById(R.id.decContent);

        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setVisibility(View.GONE);

        if (savedInstanceState != null) {
            decodedText = savedInstanceState.getString(DECODED_TEXT);
        }

        if (!decodedText.equals("")) {
            decText.setVisibility(View.VISIBLE);
            decContent.setVisibility(View.VISIBLE);
            myToolbar.setVisibility(View.VISIBLE);
            decContent.setText(decodedText);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString(DECODED_TEXT, decodedText);

        super.onSaveInstanceState(savedInstanceState);
    }

    public void history(View v) {
        if (v.getId() == R.id.bHistory) {
            Intent intent = new Intent(this, HistoryActivity.class);
            startActivity(intent);
        }
    }

    public void gallery(View v) {


        try {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, SELECTED_PICTURE);
            } else {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, SELECTED_PICTURE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu resource file.
        getMenuInflater().inflate(R.menu.menu2, menu);

        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.menu_item_share);

        // Fetch and store ShareActionProvider
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);

        // Return true to display menu
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.shareText) + decContent.getText());
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Share to"));
                setShareIntent(sendIntent);
                break;
            default:
                break;
        }
        return true;
    }


    // Call to update the share intent
    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }

    //product qr code mode
    public void scanQR(View v) {
        if (v.getId() == R.id.bScan) {

            try {
                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, RC_BARCODE_CAPTURE);
                } else {
                    Intent scanIntent = new Intent(this, BarcodeCaptureActivity.class);
                    scanIntent.putExtra(BarcodeCaptureActivity.AutoFocus, true);

                    startActivityForResult(scanIntent, RC_BARCODE_CAPTURE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            scanQrFromCamera(resultCode, data);
        } else if (requestCode == SELECTED_PICTURE) {
            decodeQrFromGallery(resultCode, data);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    private void decodeQrFromGallery(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (data != null) {

                Uri uri = data.getData();
                String[] projection = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(projection[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();

                File image = new File(filePath);
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);

                bitmap = Bitmap.createScaledBitmap(bitmap, 300, 300, false);
                BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(getApplicationContext()).setBarcodeFormats(Barcode.QR_CODE).build();

                Frame frame = new Frame.Builder().setBitmap(bitmap).build();

                SparseArray<Barcode> barsCode = barcodeDetector.detect(frame);
                if (barsCode.size() != 0) {
                    Barcode result = barsCode.valueAt(0);

                    myToolbar.setVisibility(View.VISIBLE);
                    decText.setVisibility(View.VISIBLE);
                    decContent.setText(result.rawValue);
                    decContent.setVisibility(View.VISIBLE);

                    String textToWrite = getCurrentTimeDate() + " " + result.rawValue + "\n";


                    FileOutputStream outputStream;
                    String file = "results";
                    try {
                        outputStream = openFileOutput(file, Context.MODE_APPEND);
                        outputStream.write(textToWrite.getBytes());
                        outputStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Log.d(TAG, "Barcode read: " + result.rawValue);
                } else {
                    myToolbar.setVisibility(View.INVISIBLE);
                    decText.setVisibility(View.INVISIBLE);
                    decContent.setVisibility(View.INVISIBLE);
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                    builder1.setMessage("Image is not a qr code");
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                    Log.d(TAG, "No barcode decoded, image from gallery is not a qr code image");
                }

            } else {
                Log.d(TAG, "No barcode decoded, intent data is null");
            }

        }
    }

    private void scanQrFromCamera(int resultCode, Intent data) {
        if (resultCode == CommonStatusCodes.SUCCESS) {
            if (data != null) {
                myToolbar.setVisibility(View.VISIBLE);

                Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                decodedText = barcode.displayValue;
                decContent.setText(barcode.displayValue);
                decText.setVisibility(View.VISIBLE);
                decContent.setVisibility(View.VISIBLE);

                String textToWrite = getCurrentTimeDate() + " " + decodedText + "\n";


                FileOutputStream outputStream;
                String file = "results";
                try {
                    outputStream = openFileOutput(file, Context.MODE_APPEND);
                    outputStream.write(textToWrite.getBytes());
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.d(TAG, "Barcode read: " + barcode.displayValue);
            } else {
                Log.d(TAG, "No barcode captured, intent data is null");
            }
        } else {
            decText.setText(String.format(getString(R.string.barcode_error),
                    CommonStatusCodes.getStatusCodeString(resultCode)));
        }
    }

    public static String getCurrentTimeDate() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        return sdfDate.format(now);
    }

}
